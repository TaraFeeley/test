FROM codercom/code-server:latest

USER root

RUN apt-get update && apt-get install -y sudo && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN sudo apt-get update && \
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    git \
    docker.io \
    docker-compose && \
    sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/*

RUN sudo apt-get update && \
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    python3-pip && \
    sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/*

RUN pip3 install --no-cache-dir \
    pylint \
    black \
    virtualenv

RUN code-server --install-extension ms-python.python && \
    code-server --install-extension ms-vscode-remote.remote-containers && \
    code-server --install-extension ms-azuretools.vscode-docker && \
    code-server --install-extension njpwerner.autodocstring && \
    code-server --install-extension k--kato.docomment && \
    code-server --install-extension grapeot.ansible && \
    code-server --install-extension visualstudioexptteam.vscodeintellicode && \
    code-server --install-extension njpwerner.dash && \
    code-server --install-extension ms-toolsai.jupyter

RUN sudo apt update -y

RUN apt install openjdk-17-jdk openjdk-17-jre -y

CMD ["code-server", "--auth", "password", "--port", "8443", "--disable-telemetry", "--user-data-dir", "/home/coder/.local/share/code-server/UserData"]

